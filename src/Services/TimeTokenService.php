<?php

namespace A4Sex\Services;

use Psr\Cache\CacheItemPoolInterface;

class TimeTokenService
{
    public const DEFAULT_DELAY = 2;
    public const TOKEN_EXPIRE = 10;
    private const TIMER_PREFIX = 'timer_start_identifier_';

    public function __construct(
        private readonly CacheItemPoolInterface $memory,
        private ?int $delay = null,
        private ?int $expire = null
    ) {}

    public function setExpire($time): static
    {
        $this->expire = $time;

        return $this;
    }

    public function setDelay($time): static
    {
        $this->delay = $time;

        return $this;
    }

    public function expires($expect, $expire = null): int
    {
        return $expect + ($expire ?? $this->expire ?? self::TOKEN_EXPIRE);
    }

    public function delay($time = null): int
    {
        return $time ?? $this->delay ?? self::DEFAULT_DELAY;
    }

    public function save($id, $expect, $expire): void
    {
        $cacheItem = $this->memory->getItem(self::TIMER_PREFIX . $id);
        $cacheItem->set($expect);
        $cacheItem->expiresAfter($this->expires($expect, $expire));
        $this->memory->save($cacheItem);
    }

    public function expect($id): ?int
    {
        return $this->memory->getItem(self::TIMER_PREFIX . $id)->get();
    }

    public function start($id, $delay = null, $expire = null): int
    {
        $delay = $this->delay($delay);
        $expect = time() + $delay;
        $this->save($id, $expect, $expire);

        return $delay;
    }

    public function bump($id, $delay = null, $expire = null): int
    {
        return $this->start($id, $delay, $expire);
    }

    public function restore($id, $delay = null): ?int
    {
        $delay = $this->delay($delay);
        $expect = $this->expect($id);
        if ($expect) {
            $delay = $expect - time();
        }

        return $delay;
    }

    public function left($id): ?int
    {
        $expect = $this->expect($id);
        if (!$expect) {
            return null;
        }

        return time() - $expect;
    }

    public function ready($id): ?bool
    {
        $left = $this->left($id);
        if (is_null($left)) {
            return null;
        }

        return $left >= 0;
    }

}
